# NOTA_ARQ: El cliente está integramente elaborado en un solo archivo.
import requests
import os

# NOTA_ARQ: Ejemplo de ligador, el cual se presentan también en el servidor. Todo está acotado y definido en él,
# sin que sea posible realizar un cambio sin afectar a la otra parte. Ej: Cambiar numeración de operaciones.
# NOTA_ARQ: También es un monolito de asignacón pues en caso de presentarse un cambio de nomenclatura en el servidor debe cambiarse el cliente al tiempo.
OPERACION = {
    1: "Suma",
    2: "Resta",
    3: "Multiplicación",
    4: "División"
}
class Server:
    def operacion(self, tipo_operacion, a, b):
        try:
            payload= { "tipo_operacion": tipo_operacion, "parametro1": a, "parametro2": b }
            # NOTA_ARQ: Ligado al único servidor disponible, contrario a utilizar una configuración descentralizada del servidor.
            r = requests.get('http://127.0.0.1:5000/operacion', params=payload)
            r = r.json()
            if (r['exito']):
                return f"El resultado de la {OPERACION[tipo_operacion]} es {r['resultado']}"
            else:
                return r['error']
        except:
            return "Se ha presentado un error de comunicación con el servidor"
      
def Menu():
    print ("""
    ************
    Calculadora
    ************
    Menu
    1) Suma
    2) Resta
    3) Multiplicacion
    4) Division
    5) Salir""")
def Calculadora():
    server = Server()
    Menu()
    opc = 1
    while (opc >0 and opc <5):
        try:
            Menu()
            opc = int(input("-----------------------------------------\nSelecione Opción\n"))
            if (opc >0 and opc <5):
                x = int(input("Ingrese Numero1\n"))
                y = int(input("Ingrese Numero2\n"))
                # NOTA_ARQ: Ligado a un número de operaciones definido, que de cambiar afectaría ambos componentes (cliente, servidor).
                if (opc in [1,2,3,4]):
                    print(server.operacion(opc, x, y))
                    os.system("pause")
        except ValueError:
            print("Has ingresado un valor inválido")
            return
        except Exception as e:
            print("Se ha presentado un error desconocido", repr(e))
            return
          
Calculadora()