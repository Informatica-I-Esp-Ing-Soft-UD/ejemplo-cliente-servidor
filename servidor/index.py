# NOTA_ARQ: El servidor está integramente elaborado en un solo archivo.
# NOTA_ARQ: Es un monolito de ejecución, pues es la única aplicación que contiene toda la lógica del sistema.
# NOTA_ARQ: También es un monolito de asignación pues en caso de presentarse un cambio debe cambiar también el cliente.
from flask import Flask, request
app = Flask(__name__)

class Calculadora:
    def sumar(self, numero1, numero2):
        return numero1 + numero2
    # NOTA_ARQ: Símil a las fuertes dependencias que se presentan muchas veces en módulos monolíticos.
    def restar(self, numero1, numero2):
        return self.sumar(numero1, -numero2)

    def multiplicar(self, numero1, numero2):
        return numero1 * numero2

    def dividir(self, numero1, numero2):
        if (numero2 == 0):
            raise ValueError(f"División por cero en {numero1}/{numero2}")
        else:
            return numero1 / numero2

calculadora = Calculadora()
# NOTA_ARQ: Ejemplo de ligador, el cual se presentan también en el cliente. Todo está acotado y definido en él,
# sin que sea posible realizar un cambio sin afectar a la otra parte. Ej: Cambiar numeración de operaciones.
operaciones = {
    1: calculadora.sumar,
    2: calculadora.restar,
    3: calculadora.multiplicar,
    4: calculadora.dividir
}

@app.route('/')
def saludar():
    return '¡Hola! Servidor de la calculadora'

@app.route('/operacion')
def operacion():
    try:
        tipo_operacion = int(request.args.get('tipo_operacion'))
        parametro1 = float(request.args.get('parametro1'))
        parametro2 = float(request.args.get('parametro2'))       

        return {
                "exito": True,
                "resultado": operaciones[tipo_operacion](parametro1, parametro2)
            }
    except KeyError:
        return {
            "exito": False,
            "error": "Operacion no implementada"
        }

    except Exception as e:
        return {
            "exito": False,
            "error": str(e)
        }