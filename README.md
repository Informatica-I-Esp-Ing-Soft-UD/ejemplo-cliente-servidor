# CALCULADORA CLIENTE SERVIDOR
Prototipo de una arquitectura de cliente/servidor para una calculadora de datos de tipo flotante, con operaciones de suma, resta, multiplicación y división. La misma tiene como objetivo plasmar errores en la arquitectura. 

Trabajo elaborado por: Deivy Forero, Jorge Sánchez y Edison Calderón para la asignatura de Informática I, Especialización en Ingeniería de Software de la Universidad Distrital Francisco José de Caldas, 2021.

## ¿Cómo usarla?

### Crear Entorno Virtual
Para crear un entorno virtual Se debe ejecutar el siguiente comando:

```
py -3 -m venv venv
source venv/Scripts/activate

```
### Instalar Dependencias
Usamos PIP para instalar las dependencias definidas en el archivo requirements.txt. Corremos el siguiente comando:

```
pip install -r requirements.txt
```

## Sobre la Arquitectura
La aplicación fue construida utilizando una **Arquitectura Cliente-Servidor**, ambos implementados en Python, y se comunican a través de un API Rest expuesta por el servidor, donde se expone un método para la realización de operaciones.
### Sobre los errores de la Arquitectura
Se han cometido errores intencionales en la elaboración de la arquitectura, los cuales pueden encontrarse con la etiqueta
```
# NOTA_ARQ:
```

![Búsqueda Etiqueta](/images/notaarq.png)
## Como ejecutar el servicio
Para ejecutar el cliente en una terminal ejecutar el comando 
```
sh run_local_client.sh
```

Para ejecutar el servidor en una terminal ejecutar el comando 
```
sh run_local_server.sh
```
![Ejemplo de ejecución](/images/run.png)